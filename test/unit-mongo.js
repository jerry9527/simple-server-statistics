var Mongo = require('../lib/module').Database.Mongo;

var mongo_module = require('mongodb');
var db_connect = null;

module.exports = {

    simple: {

        connect: function(test) {
            db = new mongo_module.Db('ftr-saui-reference', new mongo_module.Server('localhost', 27017));

            db.open(function(err, db) {
                if(err) {
                    test.ok(false, err);
                    test.done();
                    return;
                }
                db_connect = db;
                test.ok(true);
                test.done();
            });
        },

        get_first_time: function(test) {
            if(!db_connect) {
                test.ok(false);
                test.done();
                return;
            }

            Mongo.Mongo.Sampler.takeStatsForDb(db_connect, function(err_info, stats) {
                console.log(stats);
                test.ok(true);
                test.done();
            });
        }
    }
};