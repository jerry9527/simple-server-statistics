var CPU = require('../lib/module').CPU;

module.exports = {

    simple: {

        get_first_time: function(test) {
            CPU.Sampler.process(function(cpu_info) {
                console.log(cpu_info);
                test.ok(true);
                test.done();
            });
        },

        get_second_time: function(test) {
            CPU.Sampler.process(function(cpu_info) {
                console.log(cpu_info);
                test.ok(true);
                test.done();
            });
        }
    }
};