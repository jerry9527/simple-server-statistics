/// <reference path="../typings/node.d.ts"/>
var async = require('async');
var diskspace = require('diskspace');
var Disk;
(function (Disk) {
    var Sampler = (function () {
        function Sampler() {
        }
        Sampler.getDiskSpace = function (drives, callback2) {
            var jobs = [];
            drives.forEach(function (drive) {
                jobs.push(function (callback) {
                    diskspace.check(drive, function (err, total, free) {
                        if (err) {
                            callback();
                            return;
                        }
                        if (!total || !free) {
                            callback();
                            return;
                        }
                        try {
                            callback(null, {
                                drive: drive,
                                total: parseInt(total),
                                free: parseInt(free),
                                total_formatted: Sampler.bytesToSize(parseInt(total)),
                                free_formatted: Sampler.bytesToSize(parseInt(free)),
                                percent_free: parseFloat(((free / total) * 100).toFixed(2))
                            });
                        }
                        catch (ex) {
                            callback();
                        }
                    });
                });
            });
            async.series(jobs, function (err, results) {
                if (err == null) {
                    callback2(null, results.filter(function (result) {
                        return result != null;
                    }));
                }
                else {
                    callback2(err);
                }
            });
        };
        Sampler.bytesToSize = function (bytes) {
            if (bytes == 0) {
                return '0 bytes';
            }
            var sizes = ['n/a', 'bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
            var i = +Math.floor(Math.log(bytes) / Math.log(1024));
            return (bytes / Math.pow(1024, i)).toFixed(i ? 1 : 0) + ' ' + sizes[isNaN(bytes) ? 0 : i + 1];
        };
        return Sampler;
    })();
    Disk.Sampler = Sampler;
})(Disk = exports.Disk || (exports.Disk = {}));
//# sourceMappingURL=disk.js.map