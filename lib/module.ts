/// <reference path="../typings/node.d.ts"/>

import cpu = require('./cpu');
import disk = require('./disk');
import info = require('./info');
import mongo = require('./database/mongo');

module.exports = {

    CPU: cpu.CPU,
    Disk: disk.Disk,
    Info: info.Info,
    Database: {
        Mongo: mongo.Mongo
    }

};