/// <reference path="../../typings/node.d.ts"/>

var async = require('async');

export module Mongo {

    export class Sampler {

        private static database_stats = {};
        private static max_records_per_collection = 2;

        public static takeStatsForDb(db, callback2) {
            //Sampler.runWhenDbLoaded(db, function() {
                db.stats(function(err_db_stats, db_stats) {

                    if(err_db_stats) {
                        callback2(err_db_stats);
                        return;
                    }

                    db_stats.timestamp = new Date();

                    // make sure the place for the stats is allocated
                    Sampler.database_stats[db_stats.db] = (Sampler.database_stats[db_stats.db] ? Sampler.database_stats[db_stats.db] : {stats: [], collections: {}});
                    var db_report = Sampler.database_stats[db_stats.db];

                    // add the new stats to the stats record for the DB
                    db_report.stats.push(db_stats);
                    if(db_report.stats.length > Sampler.max_records_per_collection) {
                        db_report.stats.shift();
                    }

                    db.collections(function(err_snap, collections) {
                        if(err_snap) {
                            callback2(err_snap);
                            return;
                        }

                        var tasks = [];
                        collections.forEach(function(collection) {

                            if(collection.collectionName.indexOf('system.') != -1) {
                                return;
                            }

                            var collection_to_stat = collection;
                            tasks.push(function(callback) {
                                collection.stats(function(err_stats, stat_result) {
                                    if(err_stats) {
                                        callback(err_stats);
                                        return;
                                    }
                                    if(!db_report.collections[collection_to_stat.collectionName]) {
                                        db_report.collections[collection_to_stat.collectionName] = [];
                                    }
                                    var collection_stats = db_report.collections[collection_to_stat.collectionName];

                                    var snapshot = stat_result; //_.pick(stat_result, ['avgObjSize', 'count', 'size', 'storageSize', 'indexSizes']);
                                    snapshot.timestamp = new Date();
                                    collection_stats.push(snapshot);
                                    if(collection_stats.length > Sampler.max_records_per_collection) {
                                        collection_stats.shift();
                                    }

                                    callback(null, snapshot);
                                });
                            });
                        });
                        async.series(tasks, function(err_tasks, task_results) {
                            callback2(err_tasks, task_results);
                        });
                    });
                });
            //});
        }

        public static getStats() {
            return Sampler.database_stats;
        }

        private static runWhenDbLoaded(databaseInstance, functionToRun) {
            if(databaseInstance && databaseInstance._state == 'connected') {
                functionToRun();
                return;
            }

            setTimeout(function() {
                Sampler.runWhenDbLoaded(databaseInstance, functionToRun);
            }, 3000);
        }
    }
}