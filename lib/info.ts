/// <reference path="../typings/node.d.ts"/>

import os = require('os');

export module Info {

    export class Report {

        public static generate(result_callback) {
            result_callback({
                hostname: os.hostname(),
                type: os.type(),
                platform: os.platform(),
                architecture: os.arch(),
                uptime: os.uptime(),
                process_uptime: process.uptime(),
                node_env: process.env['ENV'],
                total_memory: os.totalmem(),
                free_memory: os.freemem(),
                versions: process.versions,
                release: os.release(),
                app_instance: process.env['APP_INSTANCE']
            });
        }
    }
}