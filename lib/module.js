/// <reference path="../typings/node.d.ts"/>
var cpu = require('./cpu');
var disk = require('./disk');
var info = require('./info');
var mongo = require('./database/mongo');
module.exports = {
    CPU: cpu.CPU,
    Disk: disk.Disk,
    Info: info.Info,
    Database: {
        Mongo: mongo.Mongo
    }
};
//# sourceMappingURL=module.js.map