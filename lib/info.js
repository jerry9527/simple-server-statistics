/// <reference path="../typings/node.d.ts"/>
var os = require('os');
var Info;
(function (Info) {
    var Report = (function () {
        function Report() {
        }
        Report.generate = function (result_callback) {
            result_callback({
                hostname: os.hostname(),
                type: os.type(),
                platform: os.platform(),
                architecture: os.arch(),
                uptime: os.uptime(),
                process_uptime: process.uptime(),
                node_env: process.env['ENV'],
                total_memory: os.totalmem(),
                free_memory: os.freemem(),
                versions: process.versions,
                release: os.release(),
                app_instance: process.env['APP_INSTANCE']
            });
        };
        return Report;
    })();
    Info.Report = Report;
})(Info = exports.Info || (exports.Info = {}));
//# sourceMappingURL=info.js.map