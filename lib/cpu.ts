/// <reference path="../typings/node.d.ts"/>

import os = require('os');

export module CPU {

    export class Sampler {

        private static cpu_stats = [];
        private static cpu_summaries = [];
        private static max_cpu_stats = 3;
        private static total_cpu_summaries = 0;
        private static max_cpu_summaries = 300;

        public static process(result_callback) {
            var prior_cpu_stats, current_cpu_stats, cpus_snapshot = {cores: [], timestamp: (new Date()).getTime()};

            Sampler.cpu_stats.push(os.cpus());
            if(Sampler.cpu_stats.length > Sampler.max_cpu_stats) {
                Sampler.cpu_stats = Sampler.cpu_stats.slice(1);
            }
            if(Sampler.cpu_stats.length > 1) {
                current_cpu_stats = Sampler.cpu_stats[Sampler.cpu_stats.length - 1];
                prior_cpu_stats = Sampler.cpu_stats[Sampler.cpu_stats.length - 2];
                for(var i=0; i<current_cpu_stats.length; i++) {
                    var idle_since_last = current_cpu_stats[i].times.idle - prior_cpu_stats[i].times.idle;
                    var total_since_last = 0, percent_used = '0';

                    Object.keys(current_cpu_stats[i].times).forEach(function(type) {
                        total_since_last += (current_cpu_stats[i].times[type] - prior_cpu_stats[i].times[type]);
                    });
                    if(total_since_last > 0) {
                        percent_used = (100 - (100 * idle_since_last / total_since_last)).toFixed(2);
                    }
                    cpus_snapshot.cores.push({pctUsed: percent_used});
                }
                Sampler.total_cpu_summaries++;
                Sampler.cpu_summaries.push(cpus_snapshot);
                if(Sampler.cpu_summaries.length > Sampler.max_cpu_summaries) {
                    Sampler.cpu_summaries = Sampler.cpu_summaries.slice(1);
                }
            }

            result_callback(cpus_snapshot);
        }

        public static getSummaries() {
            return Sampler.cpu_summaries;
        }
    }
}